﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Assets.Scripts.PlayerEngine
{
    public class HealthGUI : MonoBehaviour
    {
        private GameObject _playerHUD;
        private GameObject _screenText;
        public UnityEngine.Object ImageTemplate;

        private List<GameObject> _images = new List<GameObject>();

        public void UpdateHealth(int healthPoints)
        {
            if (_playerHUD == null)
            {
                _images = new List<GameObject>();

                if (!FindCanvasElements())
                    return;
            }

            if (healthPoints < 0)
                return;

            while (_images.Count > healthPoints)
            {
                var image = _images[_images.Count - 1];

                _images.Remove(image);
                Destroy(image.gameObject);
            }

            while (healthPoints > _images.Count)
            {
                var x = (_images.Count*75) + 100;

                var image = (GameObject)Instantiate(ImageTemplate, transform.position, Quaternion.identity);
                image.transform.position = new Vector3(x, 0);

                _images.Add(image);

                image.transform.SetParent(_playerHUD.transform, false);
            }                
        }

        public void UpdateScreenText(string newText)
        {
            if (_screenText == null)
            {
                if (!FindCanvasElements())
                    return;
            }

            var text = _screenText.GetComponent<Text>();
            text.text = newText;
        }

        private bool FindCanvasElements()
        {
            _playerHUD = GameObject.Find("PlayerUI");
            _screenText = GameObject.Find("ScreenText");

            return _playerHUD != null && _screenText != null;
        }
    }
}
