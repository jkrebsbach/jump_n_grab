﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using Assets.Scripts.Environment;
using Assets.Scripts.GameEngine;
using Assets.Scripts.NPC;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.PlayerEngine
{
    public class Player : NetworkBehaviour, IMovingCharacter
    {
        public int WalkSpeed = 10;
        public int JumpAirDrag = 5;
        public float Gravity = 3;
        public int JumpSpeed = 10;
        public int MaxHitPoints = 2;
        public int DamageBlinkSeconds = 3;
        public float Smooth = 15;
        public Texture HitPointTexture;

        public AudioClip Jump;
        public AudioClip Grab;
        public AudioClip Hurt;
        public AudioClip CollectItem;
        public GUIStyle buttonStyle = null;

        private GameObject _platform = null;
        private GameObject _backgroundLadder = null;
        private GameObject _backgroundDoor = null;
        private GameObject _ladder = null;
        private Vector2 _networkPlayerPosition = Vector2.zero;

        private PlayerAction _currentAction;
        private Rigidbody2D _rigidBody;
        private Collider2D _collider;
        private HealthGUI _healthGui;
        private ObjectThrower _objectThrower;

        private int _walkRight;
        private int _walkLeft;
        private int _climb;
        private int _stand;
        private int _hitPoints;

        private DateTime _damageWindow = DateTime.MinValue;
        private AudioSource _audioSource;
        private GameManager _gameManager;
        private float _xAxis = 0f;
        private float _yAxis = 0f;
        private bool _jumpPressed = false;
        private bool _actionPressed = false;
        private bool _inputFrozen = false;
        private bool _death = false;
        private float _deathTime = 0f;
        private bool _initialized = false;

        public enum PlayerAction { standing, walking, pickingUp, jumping, climbing };

        private FacingDirection _currentDirection;

        public Animator Animator;
        public FacingDirection CurrentDirection
        {
            get { return _currentDirection; }
            set
            {
                _currentDirection = value;
                if (isLocalPlayer)
                    CmdTurnPlayer(_currentDirection);
            }
        }

        public int Width
        {
            get { return 1; }
        }

        [Command]
        void CmdTurnPlayer(FacingDirection newDirection)
        {
            if (_currentDirection != newDirection)
            {
                CurrentDirection = newDirection;
                Animator.CrossFade(CurrentDirection == FacingDirection.right ? _walkRight : _walkLeft, 0f);
            }

            RpcTurnPlayer(newDirection);
        }

        [ClientRpc]
        void RpcTurnPlayer(FacingDirection newDirection)
        {
            if (isLocalPlayer)
                return;

            if (_currentDirection != newDirection)
            {
                CurrentDirection = newDirection;
                Animator.CrossFade(CurrentDirection == FacingDirection.right ? _walkRight : _walkLeft, 0f);
            }
        }

        public PlayerAction CurrentAction
        {
            get
            {
                return _currentAction;
            }
        }

        public bool Holding
        {
            get
            {
                return (_objectThrower.CarryingObject);
            }
        }

        public int HitPoints
        {
            get { return _hitPoints; }
            set
            {
                if (_hitPoints != value)
                {
                    _hitPoints = value;

                    if (_healthGui == null)
                    {
                        _healthGui = GetComponent<HealthGUI>();
                    }
                }

                if (_healthGui != null)
                {
                    _healthGui.UpdateHealth(_hitPoints);
                }
            }
        }

        void Start()
        {
            CurrentDirection = FacingDirection.right;
            Animator = GetComponent<Animator>();
            _currentAction = PlayerAction.standing;

            _audioSource = GetComponent<AudioSource>();
            _rigidBody = GetComponent<Rigidbody2D>();
            _collider = GetComponent<Collider2D>();
            _objectThrower = GetComponent<ObjectThrower>();
            
            _climb = Animator.StringToHash("Climb");
            _walkLeft = Animator.StringToHash("WalkLeft");
            _walkRight = Animator.StringToHash("WalkRight");
            _stand = Animator.StringToHash("Stand");


            _gameManager = GameManager.instance;
            
            HitPoints = MaxHitPoints;
            DontDestroyOnLoad(gameObject);

            if (NetworkManager.singleton != null)
            {
                foreach (var prefab in NetworkManager.singleton.spawnPrefabs)
                {
                    ClientScene.RegisterPrefab(prefab);
                }

                Connect();

                if (isLocalPlayer)
                {
                    StartCoroutine(UpdatePosition());
                }
                else
                {
                    // switch rigidbody to kinematic so it accepts network position & does not use gravity
                    _rigidBody.isKinematic = true;
                }
            }

            GameManager.instance.TestStageCompletion();
        }

        private bool _wasStanding = false;

        // Update is called once per frame
        void Update()
        {
            if (!isLocalPlayer)
            {
                LerpPosition();
                return;
            }

            if (HitPoints < 0)
            {
                Death();
            }

            if (_death)
            {
                _deathTime += Time.deltaTime;

                if (_deathTime < .25f)
                    return;

                Respawn();
            }

            if (_inputFrozen)
                return;

            // Make sure object is fully initialized
            if (_rigidBody == null)
                return;

            var keyboardMovement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            var mouseMovement = new Vector2(_xAxis, _yAxis);
            var movementVector = (keyboardMovement == Vector2.zero ? mouseMovement : keyboardMovement);

            var momentum = _rigidBody.velocity;
            FindPlatform();

            // lock player while he is picking up block
            if (_objectThrower.PickUpTime > DateTime.Now)
            {
                _currentAction = PlayerAction.pickingUp;
            }
            else
            {
                // Check player climbing ladder
                if (_backgroundLadder != null)
                {
                    if (Math.Abs(movementVector.y) > 0)
                    {
                        _rigidBody.gravityScale = 0;
                        _ladder = _backgroundLadder;
                        _currentAction = PlayerAction.climbing;

                        Animator.CrossFade(_climb, 0f);

                        momentum.y = 5 * movementVector.y;
                        momentum.x = 0;
                    }
                    else if (_ladder != null)
                    {
                        momentum.y = 0;
                        _currentAction = PlayerAction.climbing;
                    }
                }
                if (_backgroundDoor != null && _ladder == null)
                {
                    // Don't enter door if climbing ladder - push up to enter
                    if (movementVector.y > 0)
                    {
                        var door = _backgroundDoor.GetComponent<Door>();

                        EnterDoor(door);
                    }
                }

                if (_platform == null && _ladder == null)
                {
                    momentum.x = JumpMovePhysics(movementVector, momentum.x);
                }
                else
                {
                    momentum.x = GroundMovePhysics(movementVector, momentum.x);

                    if (_currentAction == PlayerAction.standing)
                    {
                        if (!_wasStanding)
                        {
                            Animator.CrossFade(_stand, 0f);
                            _wasStanding = true;
                        }
                    }
                    else
                    {
                        if (_wasStanding)
                            Animator.CrossFade(CurrentDirection == FacingDirection.right ? _walkRight : _walkLeft, 0f);
                        _wasStanding = false;
                    }
                }

                if (Input.GetButtonDown("Jump") || _jumpPressed)
                {
                    if (_platform != null)
                    {
                        _jumpPressed = false;
                        _currentAction = PlayerAction.jumping;

                        momentum.y = JumpSpeed;
                        ClearPlatform();
                        Animator.speed = 6;

                        if (!_audioSource.isPlaying)
                        {
                            _audioSource.clip = Jump;
                            _audioSource.Play();
                        }
                    }
                }

                if (Input.GetButtonDown("Action1") || _actionPressed)
                {
                    _actionPressed = false;

                    if (!_objectThrower.CarryingObject && _platform != null)
                    {
                        var isMovableObject = _platform.tag == "Movable Object";
                        var isPlayer = _platform.tag == "Player";

                        if (isMovableObject || isPlayer)
                        {
                            _objectThrower.PickUp(_platform, isPlayer);
                            _audioSource.PlayOneShot(Grab);
                        }
                    }
                    else if (_objectThrower.CarryingObject)
                    {
                        _objectThrower.Throw();
                    }
                }
            }

            var pos = transform.position;
            Debug.DrawLine(new Vector2(pos.x - .5f, pos.y), new Vector2(pos.x - .5f, pos.y - 1.1f));
            Debug.DrawLine(new Vector2(pos.x + .5f, pos.y), new Vector2(pos.x + .5f, pos.y - 1.1f));

            if (movementVector.x > 0 && CurrentDirection != FacingDirection.right)
            {
                CurrentDirection = FacingDirection.right;
                TurnCharacter();
            }
            else if (movementVector.x < 0 && CurrentDirection != FacingDirection.left)
            {
                CurrentDirection = FacingDirection.left;
                TurnCharacter();
            }

            _rigidBody.velocity = momentum;

            //Vector3 direction = (target.transform.position - transform.position).normalized;
            //rigidbody.MovePosition(transform.position + direction * movementSpeed * Time.deltaTime);

            //_rigidBody.MovePosition(new Vector2(pos.x, pos.y) + momentum * Time.deltaTime);
        }

        /// <summary>
        /// If we are on a platform, and the plaform moves - ride the platform
        /// </summary>
        public void RideOnPlatform(Vector3 platformMovement)
        {
            transform.position -= platformMovement;
        }

        void LerpPosition()
        {
            if (isLocalPlayer || _networkPlayerPosition == Vector2.zero)
            {
                return;
            }

            // Don't lerp a player if it is being held
            if (transform.parent != null && transform.parent.tag == "Player")
                return;

            transform.position = Vector3.Lerp(transform.position, _networkPlayerPosition, Time.deltaTime * Smooth);
        }

        IEnumerator UpdatePosition()
        {
            while (enabled)
            {
                CmdSendPosition(transform.position, _rigidBody.velocity);
                yield return new WaitForSeconds(.25f);
            }
        }

        [Command]
        public void CmdSendPosition(Vector3 position, Vector3 velocity)
        {
            
            //transform.position = position;
            //_rigidBody.velocity = velocity;
            RpcAdjustPosition(position);
        }

        [ClientRpc]
        public void RpcAdjustPosition(Vector3 position)
        {
            if (isLocalPlayer)
                return;

            _networkPlayerPosition = position;
        }


        public void HorizontalTouchpadPress(float value)
        {
            //GUI.Label(lfRect, "\u25C0");
            //GUI.Label(rtRect, "\u25B6");
            _xAxis = value;
        }
        public void VerticalTouchpadPress(float value)
        {
            //GUI.Label(upRect, "\u25B2");
            //GUI.Label(dnRect, "\u25BC");
            _yAxis = value;
        }


        public void JumpTouchpadPress(float value)
        {
            _jumpPressed = value > 0;
        }

        public void ActionTouchpadPress(float value)
        {
            _actionPressed = value > 0;
        }

        private float JumpMovePhysics(Vector2 movementVector, float momentumX)
        {
            // In the air, speed will drop 
            // when button not pressed drop is slowest
            // Pushing opposite direction will stop forward motion

            float factor = JumpAirDrag;

            // check player input
            if ((movementVector.x > 0 && momentumX > 0) ||
                (movementVector.x < 0 && momentumX < 0))
            {
                // User pressing in direction of jump - (resist drag?)
                factor = -0.1f;
            }
            else if (Math.Abs(movementVector.x) > 0)
            {
                // User pressing against direction of jump - Constantly decrease momentum
                factor *= 4;
            }
            else
            {
                // User not pushing any direction - apply default drag
                factor *= 2;
            }

            if (momentumX > 0)
            {
                momentumX -= (factor * Time.deltaTime);
                momentumX = Math.Max(momentumX, 0f);
                momentumX = Math.Min(WalkSpeed, momentumX); // Can't exceed maximum velocity
            }
            else if (momentumX < 0)
            {
                momentumX += (factor * Time.deltaTime);
                momentumX = Math.Min(momentumX, 0f);
                momentumX = Math.Max(-WalkSpeed, momentumX); // Can't exceed maximum velocity
            }
            else
            {
                // momentumX == 0...
                momentumX = 2f * movementVector.x;
            }

            return momentumX;
        }

        private float GroundMovePhysics(Vector2 movementVector, float momentumX)
        {
            // check player input
            if (Math.Abs(movementVector.x) > 0)
            {
                momentumX = WalkSpeed * movementVector.x;
                _currentAction = PlayerAction.walking;
            }
            else
            {
                _currentAction = PlayerAction.standing;
            }

            return momentumX;
        }

        private void TurnCharacter()
        {
            if (_ladder == null)
            {
                Animator.CrossFade(CurrentDirection == FacingDirection.right ? _walkRight : _walkLeft, 0f);
            }
        }

        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Ladder")
            {
                _backgroundLadder = collision.gameObject;
            }
            if (collision.gameObject.tag == "Door")
            {
                _backgroundDoor = collision.gameObject;
            }
            if (collision.gameObject.tag == "DeathTrap")
            {
                HitPoints = -1;
            }

        }

        void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Ladder")
            {
                _backgroundLadder = null;
                _ladder = null;
                _rigidBody.gravityScale = Gravity;

                if (_currentAction != PlayerAction.standing)
                    Animator.CrossFade(CurrentDirection == FacingDirection.right ? _walkRight : _walkLeft, 0f);
            }
            if (collision.gameObject.tag == "Door")
            {
                _backgroundDoor = null;
            }
        }

        // Check for collision with enemy
        protected void OnCollisionEnter2D(Collision2D collision)
        {
            FindPlatform();

            var damageDealer = collision.gameObject.GetComponent<DamageDealer>();

            if (damageDealer != null)
            {
                if (collision.gameObject != _platform)
                {
                    StartCoroutine(IgnoreCollision(_collider, collision.collider));
                    TakeDamage(1);
                }
            }
        }

        private IEnumerator IgnoreCollision(Collider2D source, Collider2D target)
        {
            Physics2D.IgnoreCollision(source, target);

            yield return new WaitForSeconds(1);
            Physics2D.IgnoreCollision(source, target, false);
        }

        private void UndoIgnore(Collider2D source, Collider2D target)
        {

        }
        
        public void ThrowMe(FacingDirection facingDirection, Collider2D throwSource)
        { }

        private bool FindPlatform()
        {
            var pos = transform.position;
            
            // Check if standing on top by looking down
            var hitLeft = Physics2D.RaycastAll(new Vector2(pos.x - .5f, pos.y), -Vector2.up, 1.1f);
            var hitRight = Physics2D.RaycastAll(new Vector2(pos.x + .5f, pos.y), -Vector2.up, 1.1f);

            foreach (var platform in hitLeft.Union(hitRight))
            {
                if (platform.collider.gameObject == gameObject)
                    continue;

                _platform = platform.collider.gameObject;
                Animator.speed = 1;

                // Trick to ride platforms on client devices
                if (!isServer)
                {
                    var platformNetworkComponent = _platform.GetComponent<Platform>();
                    if (platformNetworkComponent != null)
                    {
                        platformNetworkComponent.Rider = this;
                    }
                }

                return true;
            }

            ClearPlatform();
            return false;
        }

        public void ClearPlatform()
        {
            if (!isServer && _platform != null)
            {
                var platformNetworkComponent = _platform.GetComponent<Platform>();
                if (platformNetworkComponent != null)
                {
                    platformNetworkComponent.Rider = null;
                }
            }

            _platform = null;
        }


        [Command]
        public void CmdToggleInput(bool inputFrozen)
        {
            RpcToggleInput(inputFrozen);
        }

        

        [ClientRpc]
        void RpcToggleInput(bool inputFrozen)
        {
            if (isLocalPlayer)
            {
                _inputFrozen = inputFrozen;
            }
        }

        public void Collect(GameObject target)
        {
            _gameManager.CurrentStage.CollectObject();

            var collectedObjectId = target.GetComponent<NetworkIdentity>();
            var collectingPlayerId = this.GetComponent<NetworkIdentity>();

            CmdCollectObject(collectedObjectId.netId, collectingPlayerId.netId);
        }

        [Command]
        public void CmdCollectObject(NetworkInstanceId collectedObjectId, NetworkInstanceId collectingPlayerId)
        {
            var collectedObject = NetworkServer.FindLocalObject(collectedObjectId);
            var collectingPlayer = NetworkServer.FindLocalObject(collectingPlayerId);

            CollectObject(collectedObject, collectingPlayer);
            RpcCollectObject(collectedObject, collectingPlayer);
        }

        [ClientRpc]
        void RpcCollectObject(GameObject collectedObject, GameObject collectingPlayer)
        {
            CollectObject(collectedObject, collectingPlayer);
        }

        private void CollectObject(GameObject collectedObject, GameObject collectingPlayer)
        {
            _audioSource.PlayOneShot(CollectItem);
            Destroy(collectedObject);
        }

        public void TakeDamage(int damage)
        {
            if (DateTime.Now < _damageWindow)
            {
                return;
            }

            _damageWindow = DateTime.Now.AddSeconds(DamageBlinkSeconds);
            HitPoints -= damage;

            if (HitPoints >= 0)
            {
                _audioSource.PlayOneShot(Hurt);
            }
        }
        
        public void Connect()
        {
            var networkIdentity = gameObject.GetComponent<NetworkIdentity>();

            if (networkIdentity.connectionToServer != null)
                NetworkServer.SetClientReady(networkIdentity.connectionToServer);
        }

        public void Death()
        {
            // Strange stuff when game first starts...
            if (!_initialized)
            {
                _initialized = true;
                return;
            }

            // prevent object from being modified while Unity reloads
            // Assign player location during update cycle to prevent strangness
            _hitPoints = 0;
            _death = true;
            _deathTime = 0f;
        }

        public void Respawn()
        {
            if (isLocalPlayer)
            {
                _inputFrozen = true;

                // Reset to max health
                HitPoints = MaxHitPoints;

                var spawnPoints = FindObjectsOfType<NetworkStartPosition>();

                var spawnPoint = Vector3.zero;

                if (_gameManager != null)
                    spawnPoint = new Vector3(_gameManager.Location.x, _gameManager.Location.y, transform.position.z);

                if (spawnPoints != null && spawnPoints.Length > 0)
                {
                    spawnPoint = spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Length)].transform.position;
                }


                // check to make sure somebody isn't already there
                var spawnHit = Physics2D.Raycast(new Vector2(spawnPoint.x, spawnPoint.y), -Vector2.up, 1.0f);
                while (spawnHit.collider != null)
                {
                    spawnPoint = new Vector3(spawnPoint.x, spawnPoint.y - 1, spawnPoint.x);
                    spawnHit = Physics2D.Raycast(new Vector2(spawnPoint.x, spawnPoint.y), -Vector2.up, 1.0f);
                }


                _death = false;
                transform.position = spawnPoint;
                CmdSendPosition(transform.position, _rigidBody.velocity);

                _inputFrozen = false;
            }
        }

        void EnterDoor(Door door)
        {
            var nextScene = _gameManager.CurrentStage.NextStage;

            CmdAssignScene(nextScene, transform.position);


            // Old logic - from 2D platformer game
            //// Is this a new scene?  If so - go there now
            //if (door.TargetScene != _gameManager.CurrentStageName)
            //    CmdAssignScene(door.TargetScene, door.TargetLocation);
            //else
            //{
            //    // Just move player to new location
            //    transform.position = door.TargetLocation;
            //}
        }

        /// <summary>
        /// Trigger server to send change scene RPC
        /// </summary>
        [Command]
        void CmdAssignScene(string targetScene, Vector2 targetLocation)
        {
            RpcAssignScene(targetScene, targetLocation);
            
            NetworkServer.SetAllClientsNotReady();
            NetworkManager.singleton.ServerChangeScene(targetScene);
        }

        /// <summary>
        /// Respond to server request to change scene on client players
        /// </summary>
        [ClientRpc]
        void RpcAssignScene(string targetScene, Vector2 targetLocation)
        {
            // Update client game player variables
            _gameManager.Location = targetLocation;
            _gameManager.CurrentStageName = targetScene;
        }
    }
}