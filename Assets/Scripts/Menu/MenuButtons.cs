﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour {

    public GameObject PlayerPrefab;
    public GameObject NetworkManagerPrefab;

    /// <summary>
    /// Create an empty network game with just one player
    /// </summary>
    public void LoadSinglePlayer()
    {
        var stubEngine = Instantiate(NetworkManagerPrefab);
        var networkManager = stubEngine.GetComponent<NetworkManager>();

        var client = networkManager.StartHost();
    }

    /// <summary>
    /// Open the fancy lobby utility to determine the type of multiplayer 
    /// </summary>
    public void LoadMultiPlayer()
    {
        SceneManager.LoadScene("LobbyScene");
    }
}
