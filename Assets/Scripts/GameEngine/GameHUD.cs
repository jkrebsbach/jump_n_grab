﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Assets.Scripts.GameEngine
{
    public class GameHUD : MonoBehaviour
    {
        public UnityEngine.Object RequirementTemplate;

        public Sprite RequirementSatisfiedTexture;
        public Sprite RequirementMissingTexture;

        private GameObject _playerHUD;
        private GameObject _screenText;
        private List<GameObject> _requirements = new List<GameObject>();

        private bool _stageComplete = false;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void UpdateStageStatus(int amountComplete, int totalAmount)
        {
            if (_playerHUD == null)
            {
                _requirements = new List<GameObject>();

                if (!FindCanvasElements())
                    return;
            }

            while (_requirements.Count > totalAmount)
            {
                var image = _requirements[_requirements.Count - 1];

                _requirements.Remove(image);
                Destroy(image.gameObject);
            }

            while (totalAmount > _requirements.Count)
            {
                var x = (_requirements.Count * 75) + 400;

                var image = (GameObject)Instantiate(RequirementTemplate, transform.position, Quaternion.identity);
                image.transform.position = new Vector3(x, 0);

                _requirements.Add(image);

                image.transform.SetParent(_playerHUD.transform, false);
            }

            // Once stage is complete, there's nothing else to do
            if (amountComplete == totalAmount)
                _stageComplete = true;

            for (var index = 0; index < totalAmount; index++)
            {
                var requiremenTexture = (_stageComplete || index < amountComplete) ? RequirementSatisfiedTexture : RequirementMissingTexture;

                var requirement = _requirements[index];
                var requirementImage = requirement.GetComponent<Image>();

                requirementImage.sprite = requiremenTexture;
                // requirementImage.mainTexture = requiremenTexture;
            }
        }

        private bool FindCanvasElements()
        {
            _playerHUD = GameObject.Find("PlayerUI");
            _screenText = GameObject.Find("ScreenText");

            return _playerHUD != null && _screenText != null;
        }
    }
}