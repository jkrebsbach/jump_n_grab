﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.PlayerEngine;
using UnityEngine;

namespace Assets.Scripts.GameEngine
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance = null;

        public Vector2 Location;
        public string CurrentStageName;
        public Stage CurrentStage;

        //private Player _player;
        private GameObject _mainCamera;
        private GameHUD _gameHud;

        void Start()
        {
            _mainCamera = GameObject.Find("Main Camera");
        }

        void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);

            DontDestroyOnLoad(gameObject);
        }

        public void TestStageCompletion()
        {
            if (CurrentStage != null)
            {
                var success = CurrentStage.CheckVictory();

                if (success)
                    CurrentStage.SpawnExit();

                if (_mainCamera == null)
                    _mainCamera = GameObject.Find("Main Camera");

                if (_gameHud == null && _mainCamera != null)
                    _gameHud = _mainCamera.GetComponent<GameHUD>();

                if (_gameHud != null)
                    _gameHud.UpdateStageStatus(CurrentStage.AmountComplete, CurrentStage.NeededAmount);
            }
        }
    }
}