﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.GameEngine;
using Assets.Scripts.NPC;
using Assets.Scripts.PlayerEngine;
using UnityEngine;
using UnityEngine.Networking;

public class ObjectThrower : NetworkBehaviour {

    private Transform _carriedObject = null;
    private DateTime _pickUpTime = DateTime.MinValue;

    private IMovingCharacter _boundCharacter;

    // Use this for initialization
    void Start ()
    {
        var player = GetComponent<Player>();
        var movingObject = GetComponent<EnemyCharacter>();

        if (player != null)
            _boundCharacter = player;

        if (movingObject != null)
            _boundCharacter = movingObject;
    }

    public bool CarryingObject
    {
        get { return _carriedObject != null; }
    }

    public DateTime PickUpTime
    {
        get { return _pickUpTime; }
    }

    internal void Throw()
    {
        var carriedObjectId = _carriedObject.gameObject.GetComponent<NetworkIdentity>();
        var throwingSourceId = this.GetComponent<NetworkIdentity>();

        CmdThrowObject(carriedObjectId.netId, throwingSourceId.netId);

        _carriedObject = null; // Hands are free again
    }

    [Command]
    private void CmdThrowObject(NetworkInstanceId carriedObjectId, NetworkInstanceId throwingSourceId)
    {
        var carriedObject = NetworkServer.FindLocalObject(carriedObjectId);
        var throwingSource = NetworkServer.FindLocalObject(throwingSourceId);

        var isPlayer = carriedObject.gameObject.tag == "Player";

        if (isPlayer)
        {
            var player = carriedObject.GetComponent<Player>();
            player.CmdToggleInput(false);
        }

        RpcThrowObject(carriedObject, throwingSource, isPlayer);
        ThrowObject(carriedObject, throwingSource, isPlayer);
    }

    [ClientRpc]
    void RpcThrowObject(GameObject carriedObject, GameObject throwingSource, bool isPlayer)
    {
        ThrowObject(carriedObject, throwingSource, isPlayer);
    }

    void ThrowObject(GameObject carriedObject, GameObject throwingSource, bool isPlayer)
    {
        var stage = GameManager.instance.CurrentStage.gameObject;
        carriedObject.transform.SetParent(stage.transform); // Unparenting

        var throwingEntity = throwingSource.GetComponent<IMovingCharacter>();

        var direction = throwingEntity.CurrentDirection == FacingDirection.right ? 1 : -1;

        var throwSource = throwingSource.GetComponent<Collider2D>();

        var localThrowSubject = false;

        // Allow heavier objects to still be thrown, but not in a linear sense
        var throwTarget = carriedObject.GetComponent<IMovingCharacter>();
        throwTarget.ThrowMe(_boundCharacter.CurrentDirection, throwSource);

        if (isPlayer)
        {
            var networkBehaviour = throwTarget as NetworkBehaviour;
            if (networkBehaviour != null)
                localThrowSubject = networkBehaviour.isLocalPlayer;
        }

        var rigidBody2D = carriedObject.GetComponent<Rigidbody2D>();
        var boxCollider = carriedObject.GetComponent<BoxCollider2D>();

        if (isServer) // Server manages gravity of items - client players do not
        {
            rigidBody2D.isKinematic = false;
        }

        rigidBody2D.simulated = true; // object is physical - (can stand on object)
        
        if (boxCollider != null)
            boxCollider.enabled = true;

        if ((isPlayer && localThrowSubject) || (!isPlayer && isServer)) // when throwing a player, the player needs to "absorb" the throw force
        {
            var throwCenter = throwSource.GetComponent<Rigidbody2D>();
            carriedObject.transform.position = throwCenter.transform.position + new Vector3(direction * throwingEntity.Width, 2, 0);

            rigidBody2D.velocity = new Vector2(direction * 10, -10);
        }
    }

    public void GrabObject(GameObject target)
    {
        _carriedObject = target.transform;
    }

    internal void PickUp(GameObject target, bool isPlayer)
    {
        _boundCharacter.ClearPlatform(); // Time to fall...
        _carriedObject = target.transform;

        if (_carriedObject != null)
        {
            var carriedObjectId = target.GetComponent<NetworkIdentity>();
            var liftingSourceId = this.GetComponent<NetworkIdentity>();

            CmdPickupObject(carriedObjectId.netId, liftingSourceId.netId, isPlayer);
            _pickUpTime = DateTime.Now.AddMilliseconds(250);
        }
    }

    [Command]
    void CmdPickupObject(NetworkInstanceId carriedObjectId, NetworkInstanceId liftingSourceId, bool isPlayer)
    {
        var carriedObject = NetworkServer.FindLocalObject(carriedObjectId);
        var liftingSource = NetworkServer.FindLocalObject(liftingSourceId);

        if (isPlayer)
        {
            var carriedPlayer = carriedObject.GetComponent<Player>();
            carriedPlayer.CmdToggleInput(false);
        }

        PickupObject(carriedObject, liftingSource);
        RpcPickupObject(carriedObject, liftingSource);
    }

    [ClientRpc]
    void RpcPickupObject(GameObject carriedObject, GameObject liftingSource)
    {
        PickupObject(carriedObject, liftingSource);
    }

    void PickupObject(GameObject carriedObject, GameObject liftingSource)
    {
        var rigidBody2D = carriedObject.GetComponent<Rigidbody2D>();
        var boxCollider = carriedObject.GetComponent<BoxCollider2D>();
        if (rigidBody2D != null)
        {
            rigidBody2D.isKinematic = true;
            rigidBody2D.simulated = false;
        }
        if (boxCollider != null)
            boxCollider.enabled = false;

        var player = liftingSource.GetComponent<Player>();
        var playerDirection = (FacingDirection)player.CurrentDirection;

        carriedObject.transform.SetParent(transform);
        carriedObject.transform.localPosition = new Vector3(0, .5f, 0);
    }
}
