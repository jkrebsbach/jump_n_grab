﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.GameEngine
{
    public class CollectStage : Stage
    {
        public int RequiredObjects = 5;

        public override bool CheckVictory()
        {
            return collectedObjects >= RequiredObjects;
        }
        public override int AmountComplete
        {
            get { return collectedObjects; }
        }
        public override int NeededAmount
        {
            get { return RequiredObjects; }
        }
    }
}
