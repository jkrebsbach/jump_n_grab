﻿using UnityEngine;
using Assets.Scripts.PlayerEngine;

namespace Assets.Scripts.GameEngine
{
    public class TouchScreenSetup : MonoBehaviour
    {
        private Player _localPlayer;

        void Start()
        {
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY
            Destroy(this.gameObject);
#endif
        }

        public void HorizontalTouchpadPress(float value)
        {
            if (CheckPlayer())
            {
                _localPlayer.HorizontalTouchpadPress(value);
            }
        }
        public void VerticalTouchpadPress(float value)
        {
            if (CheckPlayer())
            {
                _localPlayer.VerticalTouchpadPress(value);
            }
        }


        public void JumpTouchpadPress(float value)
        {
            if (CheckPlayer())
            {
                _localPlayer.JumpTouchpadPress(value);
            }
        }

        public void ActionTouchpadPress(float value)
        {
            if (CheckPlayer())
            {
                _localPlayer.ActionTouchpadPress(value);
            }
        }

        private bool CheckPlayer()
        {
            if (_localPlayer == null)
            {
                var players = FindObjectsOfType<Player>();
                foreach (var player in players)
                {
                    if (player.isLocalPlayer)
                    {
                        _localPlayer = player;
                    }
                }
            }

            return _localPlayer != null;
        }
    }
}
