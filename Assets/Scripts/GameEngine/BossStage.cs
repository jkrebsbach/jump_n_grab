﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.GameEngine
{
    public class BossStage : Stage
    {
        public int BossHitPoints;


        public override bool CheckVictory()
        {
            return bossDamageDealt >= BossHitPoints;
        }
        public override int AmountComplete
        {
            get
            {
                return bossDamageDealt;
            }
        }
        public override int NeededAmount
        {
            get { return BossHitPoints; }
        }
    }
}
