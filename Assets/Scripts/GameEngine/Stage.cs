﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.PlayerEngine;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.GameEngine
{
    public abstract class Stage : MonoBehaviour
    {
        public GameObject StateExitTemplate;
        public Rect StageBounds;
        public Vector2 StageExit;
        public string NextStage;

        private bool _exitSpawned;
        protected int collectedObjects;
        protected int bossDamageDealt;

        public abstract int AmountComplete { get; }
        public abstract int NeededAmount { get; }

        public virtual void Start()
        {
            var cameraFollow = FindObjectOfType<CameraFollow>();
            var players = FindObjectsOfType<Player>();

            foreach (var player in players)
            {
                if (player.isLocalPlayer)
                {
                    player.Connect();
                    player.Death();
                }
            }

            if (GameManager.instance != null)
            {
                GameManager.instance.CurrentStage = this;

                GameManager.instance.TestStageCompletion();
            }

            if (cameraFollow != null)
                cameraFollow.LoadStage(this);
        }

        public abstract bool CheckVictory();

        public void CollectObject()
        {
            if (collectedObjects < int.MaxValue)
                collectedObjects++;

            GameManager.instance.TestStageCompletion();
        }

        public void DealBossDamage()
        {
            if (bossDamageDealt < int.MaxValue)
                bossDamageDealt++;

            GameManager.instance.TestStageCompletion();
        }

        public void SpawnExit()
        {
            if (_exitSpawned)
                return;

            var stageExitVector = new Vector3(StageExit.x, StageExit.y, transform.position.z);

            var exitGameObject = Instantiate(StateExitTemplate, stageExitVector, Quaternion.identity);
            NetworkServer.Spawn(exitGameObject);

            _exitSpawned = true;
        }
    }
}
