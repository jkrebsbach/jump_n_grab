﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.GameEngine
{
    interface IMovingCharacter
    {
        FacingDirection CurrentDirection { get; }
        int Width { get; }

        void ClearPlatform();
        void ThrowMe(FacingDirection facingDirection, Collider2D throwSource);
    }
    public enum FacingDirection { left, right };
}
