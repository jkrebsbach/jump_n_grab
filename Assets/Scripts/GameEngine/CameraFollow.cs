﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.PlayerEngine;
using UnityEngine;

namespace Assets.Scripts.GameEngine
{
    public class CameraFollow : MonoBehaviour
    {
        public Transform Target;
        public float Damping = 1;
        public float LookAheadFactor = 3;
        public float LookAheadReturnSpeed = 0.5f;
        public float LookAheadMoveThreshold = 0.1f;

        // Prevent camera from falling too low, or exceeding stage boundaries
        private Rect _stageBounds;

        // For tracking screen size and watching for changes
        private Rect _oldRect;
        private Camera _camera;
        private Bounds _viewportBounds;

        private float m_OffsetZ;
        private Vector3 m_LastTargetPosition;
        private Vector3 m_CurrentVelocity;
        private Vector3 m_LookAheadPos;

        // Use this for initialization
        private void Start()
        {
            _camera = GetComponent<Camera>();
        }

        public void LoadStage(Stage stage)
        {
            _stageBounds = stage.StageBounds;
        }

        // Update is called once per frame
        private void Update()
        {
            if (Target == null)
            {
                if (!FindTarget())
                    return;
            }

            // If screen rotates, camera size will change - keep viewport size in sync with screen size
            if (_oldRect == null || !_oldRect.Equals(_camera.pixelRect))
            {
                _viewportBounds = OrthographicBounds();
                _oldRect = _camera.pixelRect;
            }

            /// only update lookahead pos if accelerating or changed direction
            var xMoveDelta = (Target.position - m_LastTargetPosition).x;

            var updateLookAheadTarget = Mathf.Abs(xMoveDelta) > LookAheadMoveThreshold;

            if (updateLookAheadTarget)
            {
                m_LookAheadPos = LookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
            }
            else
            {
                m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime * LookAheadReturnSpeed);
            }

            var aheadTargetPos = Target.position + m_LookAheadPos + Vector3.forward * m_OffsetZ;
            var newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, Damping);

            // Check bounds against stage bounds
            if (newPos.x < _stageBounds.xMin + _viewportBounds.size.x / 2)
                newPos.x = _stageBounds.xMin + _viewportBounds.size.x / 2;
            if (newPos.x > _stageBounds.xMax - _viewportBounds.size.x / 2)
                newPos.x = _stageBounds.xMax - _viewportBounds.size.x / 2;

            if (newPos.y < _stageBounds.yMin + _viewportBounds.size.y / 2)
                newPos.y = _stageBounds.yMin + _viewportBounds.size.y / 2;
            if (newPos.y > _stageBounds.yMax - _viewportBounds.size.y / 2)
                newPos.y = _stageBounds.yMax - _viewportBounds.size.y / 2;

            transform.position = newPos;

            m_LastTargetPosition = Target.position;
        }

        private bool FindTarget()
        {
            var players = FindObjectsOfType<Player>();
            foreach (var player in players)
            {
                if (!player.isLocalPlayer) continue;

                Target = player.gameObject.transform;

                m_LastTargetPosition = Target.position;
                m_OffsetZ = (transform.position - Target.position).z;
                transform.parent = null;
            }

            return Target != null;
        }

        private Bounds OrthographicBounds()
        {
            var screenAspect = (float)Screen.width / (float)Screen.height;
            var cameraHeight = _camera.orthographicSize * 2;
            var bounds = new Bounds(
                _camera.transform.position,
                new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
            return bounds;
        }
    }
}
