﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Environment;
using UnityEngine;

namespace Assets.Scripts.GameEngine
{
    public class SwitchStage : Stage
    {
        private List<Switch> _switches;
        private bool _switchesFlipped = false;

        public override void Start()
        {
            _switches = new List<Switch>();
            foreach (var transformChild in transform)
            {
                var component = transformChild as UnityEngine.Component;
                if (component != null && component.name == "Switch")
                {
                    _switches.Add(component.gameObject.GetComponent<Switch>());
                }
            }

            base.Start();
        }

        public override bool CheckVictory()
        {
            if (!_switchesFlipped)
            {
                _switchesFlipped = _switches.All(s => s.CurrentSwitchState == Switch.SwitchState.SwitchDown);
            }

            return _switchesFlipped;
        }
        public override int AmountComplete
        {
            get
            {
                var switchesFlipped = _switches.Count(s => s.CurrentSwitchState == Switch.SwitchState.SwitchDown);
                return switchesFlipped;
            }
        }
        public override int NeededAmount
        {
            get { return _switches.Count(); }
        }
    }
}
