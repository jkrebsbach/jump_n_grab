﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace Assets.Scripts.GameEngine
{
    public class DefaultStage : Stage
    {
        public override int AmountComplete
        {
            get { return 0; }
        }
        public override int NeededAmount
        {
            get { return 1; }
        }

        public override bool CheckVictory()
        {
            return false;
        }
    }
}
