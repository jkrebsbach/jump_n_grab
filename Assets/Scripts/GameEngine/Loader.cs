﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.PlayerEngine;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.GameEngine
{
    class Loader : MonoBehaviour
    {
        public GameObject gameManager;

        void Awake()
        {
            if (GameManager.instance == null)
                Instantiate(gameManager);
        }
    }
}