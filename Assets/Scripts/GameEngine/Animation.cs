﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.PlayerEngine;

namespace Assets.Scripts.GameEngine
{
    public class Animation : MonoBehaviour {

        public int Columns = 10;
        public int Rows = 10;
        public int NumCells = 2;
        public float FramesPerSecond = 10f;

        //the current frame to display
        private Vector2 _size;
        private Player _currentPlayer;

        void Start()
        {
            //set the tile size of the texture (in UV units), based on the rows and columns
            _size = new Vector2(1f / (Columns + 0.1f), 1f / Rows);
            GetComponent<Renderer>().material.SetTextureScale("_MainTex", _size);

            _currentPlayer = (Player)FindObjectOfType(typeof(Player));
        }

        void Update()
        {
            int index = 0; // standing = default

            if (_currentPlayer.CurrentAction == Player.PlayerAction.jumping) {
                // Jump frame rate is twice as fast as walk
                index = (int)(Time.time * FramesPerSecond * 2) % NumCells;
            } else if (_currentPlayer.CurrentAction == Player.PlayerAction.walking) {
                index = (int)(Time.time * FramesPerSecond) % NumCells;
            }

            if (_currentPlayer.Holding) {
                index += 3; // Hold offset
            }

            if (_currentPlayer.CurrentAction == Player.PlayerAction.pickingUp) {
                index = 2; // freeze while picking up
            }

            int columnIndex = index % Columns;
            int rowIndex = index / Columns;

            //split into x and y indexes
            Vector2 offset = new Vector2(columnIndex * _size.x, 1.0f - _size.y - rowIndex * _size.y);

            GetComponent<Renderer>().material.SetTextureOffset("_MainTex", offset);
        }
    }
}