﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.GameEngine;
using Assets.Scripts.PlayerEngine;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.NPC
{
    public class ThrowingBoss : BossCharacter
    {
        public GameObject ThrowProjectilePrefab;
        private ObjectThrower _objectThrower;

            
        public int ThrowSpeed = 3;
        public int JumpSpeed = 8;
        private float throwInterval;
        private float jumpInterval;

        public override void Start()
        {
            _objectThrower = GetComponent<ObjectThrower>();

            base.Start();
        }

        public override void Update()
        {
            if (RigidBody == null)
            {
                RigidBody = GetComponent<Rigidbody2D>();
            }
            if (RigidBody == null)
                return;


            if (isServer)
            {
                throwInterval += Time.deltaTime;
                if (throwInterval > ThrowSpeed)
                {
                    throwInterval = 0f;

                    var projectile = Instantiate(ThrowProjectilePrefab, transform.position, Quaternion.identity);
                    NetworkServer.Spawn(projectile);

                    _objectThrower.GrabObject(projectile);
                    _objectThrower.Throw();
                }

                jumpInterval += Time.deltaTime;
                if (jumpInterval > JumpSpeed)
                {
                    jumpInterval = 0f;
                    
                    CmdShockJump();
                }
            }


            base.Update();
        }

   

        [Command]
        void CmdShockJump()
        {
            RpcShockJump();
        }

        [ClientRpc]
        void RpcShockJump()
        {
            ShockJump();
        }

        /// <summary>
        /// This is a derivation of the code in Player - necessarily trimmed down as a subset of functionality
        /// take player code and move it into a common library
        /// </summary>
        /// <param name="carriedObject"></param>
        /// <param name="throwingSource"></param>
        void ShockJump()
        {
            RigidBody.velocity = new Vector2(RigidBody.velocity.x, 6);
        }
    }
}
