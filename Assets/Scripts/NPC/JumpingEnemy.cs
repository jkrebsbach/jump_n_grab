﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.GameEngine;
using Assets.Scripts.PlayerEngine;
using UnityEngine;

namespace Assets.Scripts.NPC
{
    public class JumpingEnemy : DestroyableObject
    {
        [SerializeField]
        private Vector2 JumpSpeed = new Vector2(4, 6);
        [SerializeField]
        private Vector2 AttackSpeed = new Vector2(6, 8);
        [SerializeField]
        private float Cooldown = 1f;

        private bool _actionNeeded = false;
        private Player _playerTarget;
        private float _nextActionTime;

        public override void Start()
        {
            CurrentDirection = FacingDirection.right;
            
            _nextActionTime = Cooldown + UnityEngine.Random.Range(0, 1);

            base.Start();
        }

        public override void Update()
        {
            // Only server determines position
            if (!isServer)
                return;

            if (RigidBody == null)
            {
                RigidBody = GetComponent<Rigidbody2D>();
            }
            else
            {
                if (!BeingThrown)
                {
                    var momentum = RigidBody.velocity;
                    if (GetGround() && momentum.y == 0 && _nextActionTime < Time.time)
                    {
                        CalculatePath();

                        // When we reach a wall, turn around
                        if (FacingWall())
                            FlipDirection();
                    }
                }
            }
            base.Update();
        }

        /// <summary>
        /// Pathfinding for the slime - 
        /// jump randomly when player outside certain radius
        /// when player enters radius X, attack towards player
        /// </summary>
        private void CalculatePath()
        {
            // Do we have a player target?  If not, try to find one...
            if (_playerTarget == null)
            {
                if (!FindTarget())
                {
                    return;
                }
            }


            _nextActionTime = Cooldown + UnityEngine.Random.Range(0, 1);

            transform.localRotation =
                (RigidBody.velocity.x > 0 ? Quaternion.Euler(0, 180, 0) : Quaternion.Euler(0, 0, 0));


            var distance = transform.position.x - _playerTarget.transform.position.x;

            if (Math.Abs(distance) > 30)
                return;

            _actionNeeded = false;

            if (Math.Abs(distance) > 10) // free jump
            {
                // character approaching enemy - hop around randomly to create distraction
                //_animator.Play(_bounceAnim, -1, 0f);

                var leftRight = UnityEngine.Random.value > 0.5;
                RigidBody.velocity = new Vector2(JumpSpeed.x * (leftRight ? 1 : -1), JumpSpeed.y);
            }
            else // attacking jump
            {
                // character within attack window - attack
                //_animator.Play(_attackAnim, -1, 0f);

                RigidBody.velocity = new Vector2(AttackSpeed.x * (distance < 0 ? 1 : -1), AttackSpeed.y);
            }

            // face left or right, depending
            transform.localRotation =
                (RigidBody.velocity.x > 0 ? Quaternion.Euler(0, 180, 0) : Quaternion.Euler(0, 0, 0));
        }

        private bool FindTarget()
        {
            _playerTarget = FindObjectOfType<Player>();

            return _playerTarget != null;
        }
    }
}
