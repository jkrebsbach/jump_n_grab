﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.GameEngine;
using Assets.Scripts.PlayerEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.NPC
{
    public class WalkingEnemy : DestroyableObject
    {
        public float Speed = 10.0f;

        public override void Start()
        {
            CurrentDirection = FacingDirection.right;
        
            base.Start();
        }

        // Update is called once per frame
        public override void Update()
        {
            if (RigidBody == null)
            {
                RigidBody = GetComponent<Rigidbody2D>();
            }
            if (RigidBody == null)
                return;

            
            if (isServer && !BeingThrown)
            {
                // Keep rolling at constant velocity while not held
                Vector3 ballSpeed = GetComponent<Rigidbody2D>().velocity;

                RigidBody.velocity = CurrentDirection == FacingDirection.right ?
                    new Vector2(Speed, ballSpeed.y) :
                    new Vector2(-Speed, ballSpeed.y);


                // When we reach a wall, turn around
                if (FacingWall())
                    FlipDirection();
            }
            

            base.Update();
        }

        
    }
}