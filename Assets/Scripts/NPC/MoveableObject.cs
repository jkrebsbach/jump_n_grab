﻿using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using Assets.Scripts.Environment;
using Assets.Scripts.GameEngine;
using Assets.Scripts.PlayerEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.NPC
{
    public class MoveableObject : EnemyCharacter
    {
        private bool _beingThrown = false;
        private int _bounceNeeded = 0;
        private Collider2D _throwSource;
        private Rigidbody2D _rigidbody2D;
        private Collider2D _collider;
        public float OriginalMass { get; private set; }

        private int _playerLayer;
        private int _ignoreRaycastLayer;


        public override void Start()
        {
            _playerLayer = LayerMask.NameToLayer("Player");
            _ignoreRaycastLayer = LayerMask.NameToLayer("Ignore Raycast");

            _collider = GetComponent<Collider2D>();
            _rigidbody2D = GetComponent<Rigidbody2D>();
            OriginalMass = (_rigidbody2D == null ? 1 : _rigidbody2D.mass);
            base.Start();
        }

        public bool BeingThrown
        {
            get
            {
                return _beingThrown;
            }
        }

        public override void ThrowMe(FacingDirection facingDirection, Collider2D throwSource)
        {
            _beingThrown = true;
            _bounceNeeded = 1;
            _throwSource = throwSource;
            CurrentDirection = facingDirection;

            base.ThrowMe(facingDirection, throwSource);
        }

        public void FlipDirection()
        {
            // flip direction
            CurrentDirection = CurrentDirection == FacingDirection.right ? FacingDirection.left : FacingDirection.right;
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
        }

        protected void OnCollisionEnter2D(Collision2D collision)
        {
            if (!isServer)
                return;

            if (_beingThrown)
            {
                // Check if we can hurt something
                var collisionTarget = collision.gameObject.GetComponent<Platform>();

                if (collisionTarget != null)
                {
                    _beingThrown = false;
                    collisionTarget.HitByThrow(this);
                    return;
                }
            }

            if (_beingThrown && GetComponent<Rigidbody2D>() != null)
            {
                if (_bounceNeeded > 0)
                {
                    // Bounce object, resume throw state
                    _rigidbody2D.velocity = new Vector2((CurrentDirection == FacingDirection.right ? 1 : -1) * 10, 4);
                    _bounceNeeded--;
                }
                else
                {
                    // Restore thrown object to normal
                    var breakableObject = collision.gameObject.GetComponent<BreakableObject>();
                    if (breakableObject != null)
                        breakableObject.ThrowCollision(this);

                    _beingThrown = false;
                }
            }
        }

        protected bool FacingWall()
        {
            // if walking left, make sure we can go left
            // if walking right, make sure we can go right
            var width = Mathf.Abs(transform.localScale.x);

            var walkDirection = (CurrentDirection == FacingDirection.right ? Vector3.right : Vector3.left);
            var position = transform.position;
            position.x = position.x + ((width / 2f + 0.05f) * (CurrentDirection == FacingDirection.right ? 1f : -1f));
            position.y = position.y - (width / 2f) + .1f; // send raycast along bottom of sprite

            // walk through player - collide against anything else
            var layerMask = ~((1 << _playerLayer) | (1 << _ignoreRaycastLayer));
            var hit = Physics2D.Raycast(position, walkDirection, .05f, layerMask);


            Debug.DrawLine(position, new Vector3(position.x + walkDirection.x, position.y + walkDirection.y, position.z), Color.blue);

            if (hit)
            {
                var platform = GetGround();
                if (platform && platform.collider == hit.collider)
                    return false; // we're inside the floor - let physics push us out

                var collider = hit.collider.gameObject.GetComponent<Collider2D>();
                return (!collider.isTrigger);
            }
            return false;
        }

        protected RaycastHit2D GetGround()
        {
            var bottom = new Vector2(transform.position.x, _collider.bounds.min.y);

            var hit = Physics2D.RaycastAll(bottom, -Vector2.up, 0.05f);
            return hit.FirstOrDefault(h => h.collider != _collider);
        }
    }
}