﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.GameEngine;

namespace Assets.Scripts.NPC
{
    public abstract class BossCharacter : EnemyCharacter
    {
        public override void Start()
        {
            base.Start();
        }

        public override void Update()
        {
            base.Update();
        }

        public override void HitByThrow(MoveableObject thrownObject)
        {
            GameManager.instance.CurrentStage.DealBossDamage();
            Destroy(thrownObject.gameObject);

            // After boss receives enough damage, destroy boss
            if (GameManager.instance.CurrentStage.CheckVictory())
            {
                Destroy(this.gameObject);
            }
        }
    }
}
