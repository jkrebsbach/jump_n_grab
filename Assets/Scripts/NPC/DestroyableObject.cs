﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.NPC
{
    public class DestroyableObject : MoveableObject
    {
        public GameObject DropPrefab;

        public override void HitByThrow(MoveableObject thrownObject)
        {
            if (DropPrefab != null)
            {
                var spawnedObject = Instantiate(DropPrefab, transform.position, Quaternion.identity);
                NetworkServer.Spawn(spawnedObject);
            }

            Destroy(this.gameObject);
            Destroy(thrownObject.gameObject);
        }
    }
}
