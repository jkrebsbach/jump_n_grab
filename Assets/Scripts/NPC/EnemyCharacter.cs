﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.GameEngine;
using UnityEngine;

namespace Assets.Scripts.NPC
{
    public class EnemyCharacter : Platform, IMovingCharacter
    {
        private FacingDirection _facingDirection;

        public FacingDirection CurrentDirection
        {
            get { return _facingDirection; }
            set { _facingDirection = value; }
        }

        public void ClearPlatform()
        { }

        public virtual int Width
        {
            get { return 3; }
        }

        public virtual void ThrowMe(FacingDirection facingDirection, Collider2D throwSource)
        {
            CurrentDirection = facingDirection;

            ForceRefresh();
        }
    }
}
