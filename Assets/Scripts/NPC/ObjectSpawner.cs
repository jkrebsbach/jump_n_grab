﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.GameEngine;
using Assets.Scripts.PlayerEngine;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.NPC
{
    public class ObjectSpawner : NetworkBehaviour
    {
        public GameObject[] ObjectTemplates;
        public int SpawnDelay;
        public Vector2 LeftBound;
        public Vector2 RightBound;
        public float Speed = 10.0f;

        private DateTime _prefabTimer;
        private Rigidbody2D _rigidbody2D;

        private FacingDirection _facingDirection;

        // Use this for initialization
        void Start()
        {
            _facingDirection = FacingDirection.right;

            _prefabTimer = DateTime.MinValue;
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }

        // Update is called once per frame
        void Update()
        {
            if (!isServer)
                return;
            

            if (_prefabTimer.AddSeconds(SpawnDelay) < DateTime.Now)
            {
                var spawnedIndex = UnityEngine.Random.Range(0, ObjectTemplates.Length);
                var spawnLocation = transform.position;

                var spawnedObject = Instantiate(ObjectTemplates[spawnedIndex], spawnLocation, Quaternion.identity);
                NetworkServer.Spawn(spawnedObject);

                _prefabTimer = DateTime.Now;
            }

            // Some object spawners are stationary... 
            if (RightBound.x == LeftBound.x)
                return;

            // If not stationary, calculate spawner movement
            if (_facingDirection == FacingDirection.left && transform.localPosition.x < LeftBound.x)
            {
                _facingDirection = FacingDirection.right;
            }
            if (_facingDirection == FacingDirection.right && transform.localPosition.x > RightBound.x)
            {
                _facingDirection = FacingDirection.left;
            }

            
            _rigidbody2D.velocity = _facingDirection == FacingDirection.right ?
                new Vector2(Speed, 0) :
                new Vector2(-Speed, 0);
        }
    }
}
