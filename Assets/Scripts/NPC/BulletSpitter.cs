﻿using System;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets
{
    public class BulletSpitter : NetworkBehaviour
    {
        public GameObject bulletTemplate;
        public int BulletDelay;

        private DateTime _prefabTimer;

        // Use this for initialization
        void Start()
        {
            _prefabTimer = DateTime.MinValue;
        }

        // Update is called once per frame
        void Update()
        {
            if (!isServer)
                return;


            if (_prefabTimer.AddSeconds(BulletDelay) < DateTime.Now)
            {
                var bullet = Instantiate(bulletTemplate, transform.position, Quaternion.identity);
                NetworkServer.Spawn(bullet);
                
                _prefabTimer = DateTime.Now;
            }
        }
    }
}