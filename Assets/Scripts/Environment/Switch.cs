﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Assets.Scripts.GameEngine;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Environment
{
    public class Switch : NetworkBehaviour
    {
        private List<GameObject> _insideMe = new List<GameObject>();
        private SpriteRenderer _sprite;
        public AudioClip SwitchOn;
        public AudioClip SwitchOff;

        public Sprite UpTexture;
        public Sprite DownTexture;
        private AudioSource _audioSource;

        public enum SwitchState
        {
            SwitchUp = 1,
            SwitchDown = 2
        }

        public SwitchState CurrentSwitchState;

        void Start()
        {
            _sprite = GetComponent<SpriteRenderer>();
            _audioSource = GetComponent<AudioSource>();
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider != null && collider.gameObject != null && !_insideMe.Contains(collider.gameObject))
            {
                _insideMe.Add(collider.gameObject);
            }
            
            ToggleSwitchSprite();
        }

        void OnTriggerExit2D(Collider2D collider)
        {
            if (collider != null && collider.gameObject != null && _insideMe.Contains(collider.gameObject))
            {
                _insideMe.Remove(collider.gameObject);
            }

            ToggleSwitchSprite();
        }

        void ToggleSwitchSprite()
        {
            _sprite.sprite = (_insideMe.Count == 0 ? UpTexture : DownTexture);

            var newSwitchState = _insideMe.Count == 0 ? SwitchState.SwitchUp : SwitchState.SwitchDown;
            if (CurrentSwitchState != newSwitchState)
            {
                CurrentSwitchState = newSwitchState;
                _audioSource.PlayOneShot(CurrentSwitchState == SwitchState.SwitchUp ? SwitchOn : SwitchOff);
            }

            GameManager.instance.TestStageCompletion();
        }
    }
}
