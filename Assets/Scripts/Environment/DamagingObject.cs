﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.PlayerEngine;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Environment
{
    public class DamagingObject : NetworkBehaviour
    {
        public int DamageAmount = 1;

        internal void Start()
        {
            
        }

        internal void OnTriggerEnter2D(Collider2D collider)
        {
            // Check if we hit player
            var playerObject = collider.gameObject.GetComponent<Player>();
            if (playerObject != null)
            {
                playerObject.TakeDamage(DamageAmount);
            }
        }
    }
}
