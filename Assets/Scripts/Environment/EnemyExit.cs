﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.NPC;
using UnityEngine;

namespace Assets.Scripts.Environment
{
    public class EnemyExit : MonoBehaviour
    {

        void OnTriggerEnter2D(Collider2D collision)
        {
            var enemyCollider = collision.GetComponent<EnemyCharacter>();

            if (enemyCollider != null)
            {
                Destroy(collision.gameObject);
            }

        }
    }
}
