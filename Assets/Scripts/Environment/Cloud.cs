﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.PlayerEngine;
using UnityEngine;

namespace Assets.Scripts.Environment
{
    public class Cloud : Platform
    {
        private Rigidbody2D _cloudBody;
        public float CloudSpeed = 1;
        
        // Use this for initialization
        public override void Start()
        {
            base.Start();
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            // Check if we hit player
            var playerObject = collider.gameObject.GetComponent<Player>();
            if (playerObject != null)
            {
                // No action needed?
            }
            else if(!collider.isTrigger)
            {
                // Hit a wall - Destroy the bullet
                Destroy(this.gameObject);
            }
        }

        public override void Update()
        {
            if (_cloudBody == null)
            {
                _cloudBody = GetComponent<Rigidbody2D>();
            }
            if (_cloudBody == null)
                return;

            if (isServer)
            {
                _cloudBody.velocity = new Vector3(1 * CloudSpeed, 0);
            }

            base.Update();
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            int tmpInt = 0;
        }
    }
}
