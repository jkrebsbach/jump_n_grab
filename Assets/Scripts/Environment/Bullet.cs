﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.PlayerEngine;

namespace Assets.Scripts.Environment
{
    public class Bullet : MonoBehaviour
    {
        public float BulletSpeed = 1;
        
        // Use this for initialization
        void Start()
        {
            GetComponent<Rigidbody2D>().velocity = new Vector3(1 * BulletSpeed, 0);
        }

        // Update is called once per frame
        void OnTriggerEnter2D(Collider2D collider)
        {
            // Check if we hit player
            var playerObject = collider.gameObject.GetComponent<Player>();
            if (playerObject != null)
            {
                playerObject.TakeDamage(1);
            }
            else
            {
                if (!collider.isTrigger)
                {
                    // Hit a wall - Destroy the bullet
                    Destroy(this.gameObject);
                }
            }
        }
    }
}