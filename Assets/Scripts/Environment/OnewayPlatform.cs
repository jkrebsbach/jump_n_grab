﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Assets
{
    public class OnewayPlatform : MonoBehaviour
    {
        Collider2D parentCollider = null;

        public float maxAngle = 45.0f;
        void Start()
        {
            parentCollider = transform.parent.GetComponent<Collider2D>();
            parentCollider.isTrigger = true;
        }

        void OnTriggerEnter2D(Collider2D player)
        {
            parentCollider.isTrigger = false;
        }

        void OnTriggerExit2D(Collider2D player)
        {
            parentCollider.isTrigger = true;
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            int tmpInt = 0;
        }
    }
}