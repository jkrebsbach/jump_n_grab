﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.PlayerEngine;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Environment
{
    public class Projectile : NetworkBehaviour
    {
        public FlightPathType FlightPath;
        public int ProjectileSpeed = 2;

        private Vector2 _targetPath;
        private Rigidbody2D _rigidbody2D;
        private Player _playerTarget;

        public enum  FlightPathType
        {
            Linear = 0,
            Sinusoid = 1
        }

        void Start()
        {
            // Find path to player, never update...
            _playerTarget = FindObjectOfType<Player>();
            _rigidbody2D = GetComponent<Rigidbody2D>();

            _targetPath = PathToPlayer();
        }

        void Update()
        {
            if (isServer)
            {
                // Keep rolling at constant velocity while not held

                if (FlightPath == FlightPathType.Linear)
                {
                    _rigidbody2D.velocity = new Vector2(_targetPath.x, _targetPath.y).normalized * ProjectileSpeed;
                }
                else
                {
                    _rigidbody2D.velocity = new Vector2(_targetPath.x, _targetPath.y).normalized * ProjectileSpeed;
                }
            }
        }

        private Vector2 PathToPlayer()
        {
            var playerRigidBody = _playerTarget.GetComponent<Rigidbody2D>();

            return playerRigidBody.transform.position - _rigidbody2D.transform.position;
        }
    }
}
