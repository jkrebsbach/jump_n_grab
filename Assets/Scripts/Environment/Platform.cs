﻿using System;
using UnityEngine;
using System.Collections;
using Assets.Scripts.GameEngine;
using Assets.Scripts.NPC;
using UnityEngine.Networking;
using Assets.Scripts.PlayerEngine;

public class Platform : NetworkBehaviour
{
    [SyncVar]
    Vector3 realPosition = Vector3.zero;
    private float updateInterval;
    private float smooth = 4.2f;
    [SyncVar]
    InitializationStatus _initialization = InitializationStatus.NoPosition;

    public Player Rider;
    internal Rigidbody2D RigidBody;

    private enum InitializationStatus
    {
        NoPosition,
        NotSet,
        Ready
    }

    // Use this for initialization
    public virtual void Start () {
        // if this is not the server, transform object to kinematic -
        // only server is responsible for positioning

        RigidBody = GetComponent<Rigidbody2D>();

        if (!isServer && RigidBody != null)
        {
            RigidBody.isKinematic = true;
        }
	}


    public virtual void HitByThrow(MoveableObject thrownObject)
    { }

    protected void ForceRefresh()
    {
        updateInterval = 100f; // force position update
    }

    // Update is called once per frame
    public virtual void Update ()
    {
        // Only force update if object not attached to a player
        if (transform.parent == null || transform.parent.gameObject.tag != "Player")
        {
            if (isServer)
            {
                updateInterval += Time.deltaTime;
                if (updateInterval > 0.11f) // 9 times per second
                {
                    updateInterval = 0f;

                    CmdSync(RigidBody.transform.position);
                }
            }
            else
            {
                if (_initialization == InitializationStatus.NotSet)
                {
                    RigidBody.transform.position = realPosition;
                    _initialization = InitializationStatus.Ready;
                }
                else if (_initialization == InitializationStatus.Ready)
                {
                    var oldPosition = RigidBody.transform.position;
                    RigidBody.transform.position = Vector3.Lerp(transform.position, realPosition, Time.deltaTime * smooth);

                    var delta = oldPosition - (RigidBody.transform.position);

                    if (Rider != null)
                    {
                        // slide rider
                        Rider.RideOnPlatform(delta);
                    }
                }
            }
        }
    }

    [Command]
    void CmdSync(Vector3 position)
    {
        realPosition = position;

        if (_initialization == InitializationStatus.NoPosition)
        {
            _initialization = InitializationStatus.NotSet;
        }
    }
}
