﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.PlayerEngine;
using UnityEngine.Networking;
using UnityEngine;

namespace Assets.Scripts.Environment
{
    public class CollectibleObject : NetworkBehaviour
    {
        private GameObject _collectibleBounds;

        internal void Start()
        {
            _collectibleBounds = transform.parent.gameObject;
        }

        internal void OnTriggerEnter2D(Collider2D collider)
        {
            // Check if we hit player
            var playerObject = collider.gameObject.GetComponent<Player>();
            if (playerObject != null)
            {
                playerObject.Collect(_collectibleBounds);
            }
        }
    }
}
